import math

print("Factorial 30 mod 59 = ", math.factorial(30) % 59 )

print("\n2 power 100 mod 7 = ", 2 ** 100 % 7)

print("\nThe integer obtained by concatenating 99 9s, divided by 25 = ", int("99" * 9) / 25)

print("\nNumber of bits needed to represent 33^33 = ", int(math.log2(33 ** 33)) + 1)

print("\nNumber of digits has the decimal representation of 33^33 = ", len(str(33 ** 33)))

print("\nHighest value of the absolute values of the numbers −10, 5, 20, −35 is ", max(abs(n) for n in[-10, 5, 20, -35]))

print("\nLowest value of the absolute values of the numbers −10, 5, 20, −35 is ", min(abs(n) for n in[-10, 5, 20, -35]))

print("\nThe area of a circle with radius 3 is ", 2 * 3 * math.pi)

print("\nThe number of ±1 walks of 20 steps that start and end in 0 is ", math.comb(20 , 10))

print ("\nThe size of the human genome is approx. n = 3 100 000 00 . log2(n) is ", int(math.log2(3100000000))+1)

