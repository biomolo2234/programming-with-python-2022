"""reads the FASTA file"""
from genetic_code_functions import *
import collections
filename = 'sequence.fasta'
seq = read_fasta(filename)
print(seq)
count = collections.Counter(seq.upper())
print(count)
