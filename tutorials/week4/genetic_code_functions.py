"""Translating DNA to Protein Sequence functions"""
#read fasta format
def read_fasta(filename):
    seq = ""
    with open(filename,"rt") as file:
        for line in file:
            if line.startswith(">"):
                continue
            seq += line.strip()
    return seq

# find codon
def find_codon(seq):
    amino_dic ={
        "GCT" : "A",
        "GCC" : "A",
        "GCA" : "A",
        "GCG" : "A",
        "CGT" : "R",
        "CGC" : "R",
        "CGA" : "R",
        "CGG" : "R",
        "AGA" : "R",
        "AGG" : "R",
        "AAT" : "N",
        "AAC" : "N",
        "GAT" : "D",
        "GAC" : "D",
        "TGT" : "C",
        "TGC" : "C",
        "CAA" : "Q",
        "CAG" : "Q",
        "GAA" : "E",
        "GAG" : "E",
        "GGT" : "G",
        "GGC" : "G",
        "GGA" : "G",
        "GGG" : "G",
        "CAT" : "H",
        "CAC" : "H",
        "ATT" : "I",
        "ATC" : "I",
        "ATA" : "I",
        "CTT" : "L",
        "CTC" : "L",
        "CTA" : "L",
        "CTG" : "L",
        "TTA" : "L",
        "TTG" : "L",
        "AAA" : "K",
        "AAG" : "K",
        "ATG" : "M",
        "TTT" : "F",
        "TTC" : "F",
        "CCT" : "P",
        "CCC" : "P",
        "CCA" : "P",
        "CCG" : "P",
        "TCT" : "S",
        "TCC" : "S",
        "TCA" : "S",
        "TCG" : "S",
        "AGT" : "S",
        "AGC" : "S",
        "ACT" : "T",
        "ACC" : "T",
        "ACA" : "T",
        "ACG" : "T",
        "TGG" : "W",
        "TAT" : "Y",
        "TAC" : "Y",
        "GTT" : "V",
        "GTC" : "V",
        "GTA" : "V",
        "GTG" : "V",
        "TAA" : "STOP",
        "TGA" : "STOP",
        "TAG" : "STOP"  
        }
    return amino_dic[seq]
# search codon with start position
def search_codon(seq, codon,start):
    i = start
    if codon != "":
        while i < len(seq)-2:
            if seq[i:i+3] == codon:
                break
            i += 3
    amino = []
    while i < len(seq)-2:
        if find_codon(seq[i:i+3]) == "STOP":
            break       
        amino.append(find_codon(seq[i:i+3]))
        i += 3
    return amino, i + 3 #end








        
