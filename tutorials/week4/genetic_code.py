"""Translating DNA to Protein Sequence"""
from genetic_code_functions import *

""" searches for the first start codon ATG and translates the DNA sequence into an amino acid sequence,
up to (but excluding) the first stop codon"""    
filename = 'sequence.fasta'
seq = read_fasta(filename)
print("Amino acids that the first start codon is ATG:")
amino, end = search_codon(seq,"ATG", 0)
print(amino)

def all_valid_coding(seq):
    end = 0
    while end < len(seq) - 2:
        amino, end = search_codon(seq, "",end)
        if amino:
            print(amino)
print("translates all valid coding regions:")
all_valid_coding(seq)

    





        
