"""Here is the solution for problem02 file"""
#Examples of loops

def my_max(numbers):
    max = numbers[0]
    for number in numbers:
        if number > max:
            max = number
    return max

def process_list(numbers):
    modify_numbers = []
    for index, number in enumerate(numbers):
        if index % 7 == 0:
            modify_numbers.append(number + index)
        elif number % 2 :
            modify_numbers.append(number * 2)
        else:
            modify_numbers.append(number / 2)
    return modify_numbers

#Collatz Conjecture
def collatz(number):
    create_numbers = []
    while 1:
        if number % 2 :
            if create_numbers.find(number *3 +1) > -1 :
                return number
            number = number * 3 +1
            create_numbers.append(number)
        else:
            if create_numbers.find(number / 2) > -1 :
                return number
            number /= 2
            create_numbers.append(number)

def compute_pi(number_of_step):
    pi = 0
    for step in range(number_of_step):
        pi += 1/(2 * step + 1) * (-1 ** step)
    return pi

def integer_cube_root(number):
    k = 0
    while (k ** 3 < number):
        k += 1
    return k

#Exercises on Loops and Strings
    
