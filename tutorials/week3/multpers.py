#Warm Up 2
def multiplicative_persistence(number):
    multi_persistance = 0
    while len(str(number)) > 1:
        multi_persistance += 1
        list_digits = list(str(number))
        number = 1
        for i in list_digits:
            number *= int(i)
    return multi_persistance
    
def argest_multiplicative_persistence(number):
    largest_number = 0
    largest_multi_persistance = 0
    for i in range(number + 1):
        multi_persistance = multiplicative_persistence(i)
        if multi_persistance > largest_multi_persistance :
            largest_multi_persistance = multi_persistance
            largest_number = i
    return largest_multi_persistance, largest_number

largest_multi_persistance, largest_number = argest_multiplicative_persistence(9999)
print("The largest multiplicative persistence of all number 0 .. 9999 is", largest_multi_persistance)
print("The largest number that has this largest multiplicative persistence is", largest_number)


def test_999():
    assert multiplicative_persistence(999) == 4
def test_39():
    assert multiplicative_persistence(39) == 3
def test_8():
    assert multiplicative_persistence(8) == 0
