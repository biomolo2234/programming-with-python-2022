
import collections

#Warm Up 1
def Counting_duplicates(s):
    dup = collections.Counter(s.lower())
    count_dup = 0
    for c in dup:
        if dup[c] > 1:
            count_dup += 1
    return count_dup

input_str = input("Enter string, it contains only letters (both upper case and lower case):")
print("Number of characters that occur more than once is", Counting_duplicates(input_str))
        

        
   
