#Extracting k-mers (substrings of length k) from a DNA sequence

def list_kmers(s, k):
    all_kmers = []
    for i, char in enumerate(s):
        if k + i <= len(s):
            new_mer = ''
            for j in range(k):
                new_mer += s[i+j]
            all_kmers.append(new_mer)
    return all_kmers

def test_1():
    assert len(list_kmers("AATCGTA",7)) == 1
def test_2():
    assert len(list_kmers("AATCGTA",8)) == 0
def test_3():
    assert len(list_kmers("AATCGTA",6)) == 2
def test_4():
    assert len(list_kmers("AATCGTA",10)) == 0
