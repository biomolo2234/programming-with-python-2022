"""factorial with generator"""
def factorial_generator():
    factortial = 1
    iterator_num = 1
    while True:
        yield factortial * iterator_num
        factortial *= iterator_num
        iterator_num += 1
        
"""Triangular Numbers with Generator"""
def triangular_generator():
    triangular = 0
    iterator_num = 0
    while True:
        yield triangular + iterator_num
        triangular += iterator_num
        iterator_num += 1

F = factorial_generator()
print("Factortial seq:")
print([next(F) for _ in range(10)])
F = triangular_generator()
print("Triangular Numbers")
print([next(F) for _ in range(10)])
