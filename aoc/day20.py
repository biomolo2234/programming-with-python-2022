import copy
from typing import NamedTuple

class Item(NamedTuple):
    index: int
    value: int

orig_list = []
with open("day20.txt") as file:
    for index, line in enumerate(file):
        number = int(line.strip())
        orig_list.append(Item(index, number))

def mix(orig_list, count):
    mixed_list = copy.deepcopy(orig_list)
    #print(mixed_list)
    for _ in range(count):
        for item in orig_list:
            current_position = mixed_list.index(item)
            del mixed_list[current_position]
            new_position = (current_position+item.value) % len(mixed_list)
            mixed_list.insert(new_position, item)

    for zero_idx, item in enumerate(mixed_list):
        if item.value == 0:
            break

    sum_mixed_val = 0
    for i in [1000,2000,3000]:
        mixed_val = mixed_list[(zero_idx + i) % len(mixed_list)]
        sum_mixed_val += mixed_val.value
    print(sum_mixed_val)

mix(orig_list, 1)
list2 = [item._replace(value=item.value * 811589153) for item in orig_list]
mix(list2, 10)
