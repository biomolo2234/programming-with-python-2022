from copy import deepcopy
dic_coords = {}
moves = {
    'N' : [(-1,0), (-1,-1),(-1,1)],
    'S' : [(1,0),(1,-1),(1,1)],
    'W' : [(0,-1),(-1,-1),(1,-1)],
    'E' : [(0,1),(-1,1),(1,1)]
    }
move_new_coord = {
    'N' : (-1,0),
    'S' : (1,0),
    'W' : (0,-1),
    'E' : (0,1)
    }
def move_elves(possible_move):
    maxX = 0
    minX = len(dic_coords)
    maxY = 0
    minY = len(dic_coords)
    next_coords = {}
    dic_current_coords = deepcopy(dic_coords)
    dic_coords.clear()
    count_elf_move = 0
    for coord in dic_current_coords.keys():
        new_coord = coord
        can_move = 0
        for x in [-1,0,1]: # there are other Elves  in one of adjacent positions
            for y in [-1,0,1]:
                if (x != 0 or y != 0) and (coord[0]+ x, coord[1]+ y) in dic_current_coords.keys():
                    can_move = 1
                    break
            if can_move:
                break
        if can_move == 1:
            for char in list(possible_move):
                next_move = moves[char]
                can_move = 1
                for move in next_move:
                    coord1 = (coord[0]+ move[0], coord[1]+ move[1])
                    if coord1 in dic_current_coords:
                        # there is an elf in this point
                        can_move = 0
                        break # from for move in next_move
                if can_move:
                    new_move = move_new_coord[char]
                    new_coord = (coord[0]+ new_move[0], coord[1]+ new_move[1])
                    break #from for char in list(possible_move)
        dic_current_coords[coord] = new_coord
        if new_coord in next_coords: # other Elf moved to this position
            next_coords[new_coord] += 1
        else:
            next_coords[new_coord] = 1           

    for coord in next_coords.keys():
        if next_coords[coord] > 1:
            for key,val in dic_current_coords.items():
                if val == coord:
                    dic_coords[key] = key
        else:
            dic_coords[coord] = coord
    for coord in dic_coords.keys():
        if coord[0] > maxX:
            maxX = coord[0]
        if coord[0] < minX:
            minX = coord[0]
        if coord[1] > maxY:
            maxY = coord[1]
        if coord[1] < minY:
            minY = coord[1]
    for key,val in dic_current_coords.items():
        if key == val:
            count_elf_move += 1
    dic_current_coords.clear()
    
    return minX, maxX, minY, maxY, count_elf_move


with open("day23.txt") as file:
    lines = file.readlines()

for i,line in enumerate(lines):
    line = line.strip()
    for j, char in enumerate(list(line)):
        if char == "#":
            dic_coords[i,j] = (i,j)
maxXscan = i
maxYscan = j
possible_moves =['NSWE','SWEN','WENS','ENSW']
first_dic_coords = deepcopy(dic_coords)
for ro in range(10):
    minX, maxX, minY, maxY, _ = move_elves(possible_moves[ro%4])

print((maxX-minX+1)*(maxY-minY+1) - len(dic_coords))

#part2

count_elf_move1 = 0
dic_coords.clear()
dic_coords = first_dic_coords
ro = 0
while count_elf_move1 < len(dic_coords):
    minX, maxX, minY, maxY, count_elf_move1 = move_elves(possible_moves[ro%4])
    ro += 1

print(ro)
