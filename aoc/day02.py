dic_play_result = { # 3 is even, 6 is win, 0 is lose --- R: Rock , S: Scissor, P: Paper
    "RR" : 3, # even
    "SS" : 3, # even
    "PP" : 3, # even
    "RS" : 0, #lose
    "RP" : 6, #win
    "SR" : 6, #win
    "SP" : 0, #lose
    "PR" : 0, #lose
    "PS" : 6 #win
    }
dic_opponent = {
    "A" : "R",
    "B" : "P",
    "C" : "S"
    }
dic_reverse_opponent = {value: key for (key, value) in dic_opponent.items()}
dic_player = {
    "X" : "R",
    "Y" : "P",
    "Z" : "S"
    }
dic_score = {
    "A" : 1,
    "B" : 2,
    "C" : 3,
    "X" : 1,
    "Y" : 2,
    "Z" : 3
    }
elf_result = {
    "X" : 0,
    "Y" : 3,
    "Z" : 6
    }
elf_score = 0
elf_total_score = 0
total_scores = 0
score = 0
with open("day02.txt") as file:
    for line in file:
        results = line.split()
        score = dic_score[results[1]] + dic_play_result[dic_opponent[results[0]]+ dic_player[results[1]]]
        total_scores += score
        # elf strategy
        for i, value in dic_play_result.items():
            j = str(i)
            if (value == elf_result[results[1]]) and (j[0] == dic_opponent[results[0]]):
                elf_score = value + dic_score[dic_reverse_opponent[j[1]]]
                elf_total_score +=  elf_score


print(total_scores)
print(elf_total_score)



