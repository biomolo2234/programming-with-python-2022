
def find_common(str1, str2):
    common_char = ""
    for i in str1:
        if i in str2:
            common_char = i
            return common_char
def find_common_3(str1, str2, str3):
    common_char = ""
    for i in str1:
        if (i in str2) and (i in str3):
            common_char = i
            return common_char

sum_priority = 0
priority = 0
with open("day03.txt") as file:
    lines = file.readlines()

for line in lines:
    line = line.strip()
    len_line = len(line)
    str1 = line[0:int(len_line /2)]
    str2 = line[int(len_line /2):len_line]
    common_char = find_common(str1, str2)
    if ord(common_char) > 96:
        priority = ord(common_char)-96
    else:
        priority = ord(common_char)- 38
    sum_priority += priority
print(sum_priority)


##part 2
sum_priority = 0
for i in range(0, len(lines), 3):
    common_char = find_common_3(lines[i], lines[i+1], lines[i+2])
    if ord(common_char) > 96:
        priority = ord(common_char)-96
    else:
        priority = ord(common_char)- 38
    sum_priority += priority
print(sum_priority)
