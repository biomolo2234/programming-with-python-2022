
import collections as cl
with open("day06.txt") as file:
    line = file.readline()
line = line.strip()
for i in range(len(line)):
    common_char = cl.Counter(line[i:i+4]).most_common(1)
    if common_char[0][1] == 1:
        break
print(i+4)
#part 2
for i in range(len(line)):
    common_char = cl.Counter(line[i:i+14]).most_common(1)
    if common_char[0][1] == 1:
        break
print(i+14)



