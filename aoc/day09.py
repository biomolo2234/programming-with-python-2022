
def motion(move, head_pos, tail_pos):

    if move == 'R':
        next_head_pos = (head_pos[0], head_pos[1]+1)
        dic = abs(next_head_pos[1]- tail_pos[1])
    elif move == 'L':
        next_head_pos = (head_pos[0], head_pos[1]-1)
        dic = abs(next_head_pos[1]- tail_pos[1])
    elif move == 'U':
        next_head_pos = (head_pos[0]+1, head_pos[1])
        dic = abs(next_head_pos[0]- tail_pos[0])
    elif move == 'D':
        next_head_pos = (head_pos[0]-1, head_pos[1])
        dic = abs(next_head_pos[0]- tail_pos[0])
    
    if dic > 1:
        next_tail_pos = head_pos
    else:
        next_tail_pos = tail_pos

    return next_head_pos, next_tail_pos
    

with open("day09.txt") as file:
    lines = file.readlines()
all_tail_pos = []
head_pos = (0,0)
tail_pos = (0,0)
for line in lines:
    line = line.strip()
    move, step = line.split()
    for i in range(int(step)):
        head_pos, tail_pos = motion(move, head_pos, tail_pos)
        if tail_pos not in all_tail_pos:
            all_tail_pos.append(tail_pos)

print(len(all_tail_pos))

