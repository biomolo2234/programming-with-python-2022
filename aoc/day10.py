
with open("day10.txt") as file:
    lines = file.readlines()

cycle = 1
sum_signal = 0
val = 1
cycle_val = []
for line in lines:
    line = line.strip()
    move = line.split()
    if len(move) == 1: # noop instruction
        cycle_val.append(val)
        cycle += 1
    else: # addx instruction
        cycle_val.append(val)
        cycle_val.append(val)
        cycle += 2
        val += int(move[1])
cycle_val.append(val)

for i in [20,60,100,140,180,220]:
    sum_signal += i * cycle_val[i-1]
print(sum_signal)
#part 2
ctr_val = []
ctr_cycle = 0

for val in cycle_val:
    #print(val, ctr_cycle, ctr_cycle % 39)
    if ctr_cycle % 40 in range(val-1,val+2):
        ctr_val.append("#")
        #print("#")
    else:
        ctr_val.append(".")
        #print(".")
    ctr_cycle += 1
    #input()

print("".join(ctr_val[0:39]))
print("".join(ctr_val[40:79]))
print("".join(ctr_val[80:119]))
print("".join(ctr_val[120:159]))
print("".join(ctr_val[160:199]))
print("".join(ctr_val[200:239]))

        



    
