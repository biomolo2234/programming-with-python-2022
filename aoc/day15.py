import re
from typing import NamedTuple

match = re.compile(r"Sensor at x=([-\d]+), y=([-\d]+): closest beacon is at x=([-\d]+), y=([-\d]+)").fullmatch

class Point(NamedTuple):
    x: int
    y: int

    def distance(self, other):
        return abs(self.x - other.x) + abs(self.y - other.y)

class Sensor:
    def __init__(self, x_sensor, y_sensor, x_beacon, y_beacon):
        self.sensor = Point(int(x_sensor), int(y_sensor))
        self.beacon = Point(int(x_beacon), int(y_beacon))


def all_observations(sensors, line_y):
    observation_point = dict()
    for item in sensors:
        sensor = item.sensor
        beacon = item.beacon
        r = sensor.distance(beacon) - abs(sensor.y - line_y)
        if r >= 0:
            for i in range(sensor.x-r, sensor.x+r+1):
                observation_point[i] = 1
    for item in sensors:
        sensor = item.sensor
        beacon = item.beacon
        if sensor.y == line_y: # there is a sensor in line_r which must not to added in list of observation points
            observation_point[sensor.x] = 0
        if beacon.y == line_y: # there is a beacon in line_r which must not to added in list of observation points
            observation_point[beacon.x] = 0
    
    return sum(observation_point.values())


def observe_one_row(sensors, line_y):
    observation_point = dict()
    for item in sensors:
        sensor = item.sensor
        beacon = item.beacon
        r = sensor.distance(beacon) - abs(sensor.y - line_y)
        if r >= 0:
            for i in range(sensor.x-r, sensor.x+r+1):
                observation_point[i] = 1
    return sum(observation_point.values())

    
def find_tuning(sensors, signal_dic):
    for line_y in range(signal_dic+1):
##        coordinates = [0]*(signal_dic+1)
##        for item in sensors:
##            sensor = item.sensor
##            beacon = item.beacon
##            r = sensor.distance(beacon) - abs(sensor.y - line_y)
##            if r >= 0:
##                for j in range(max(sensor.x-r, 0), min(sensor.x+r+1,signal_dic+1)):
##                    coordinates[j] = 1
        sum_coordinates = observe_one_row(sensors, line_y)
        if sum_coordinates < signal_dic+1:
            for j in range(signal_dic+1):
                if coordinates[j] == 0:
                    return(j* 4000000 + line_y)
                    

with open("day15.txt") as file:
    sensors = []
    for line in file:
        line = line.strip()
        x_sensor, y_sensor, x_beacon, y_beacon = match(line).groups()
        sensors.append(Sensor(x_sensor, y_sensor, x_beacon, y_beacon))

print( all_observations(sensors, 2000000))
print(find_tuning(sensors,4000000))

