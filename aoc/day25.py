dic_snafu = {
    "0" : 0,
    "1" : 1,
    "2" : 2,
    "-" : -1,
    "=" : -2
    }
dic_int = {
    0 : "0",
    1 : "1",
    2 : "2",
    3 : "=",
    4 : "-"
    }
def snafu2int(snafu_num):
    len_num = len(snafu_num)
    int_num = 0
    for i, char in enumerate(snafu_num):
        int_num += 5**(len_num-i-1) * dic_snafu[char]
    return int_num

def int2snafu(int_num):
    snafu_num = ""
    while True:
        mod_num = int_num % 5
        char = dic_int[mod_num]
        snafu_num = char + snafu_num
        int_num -= dic_snafu[char]
        int_num //= 5
        if int_num == 0:
            return snafu_num

with open("day25.txt") as file:
    lines = file.readlines()
sum_int = 0
for line in lines:
    line = line.strip()
    val = snafu2int(line)
    sum_int += val
#print(sum_int)
print(int2snafu(sum_int))
