def compare(list1, list2):
    if type(list1) != list:
        list1 = [list1]
    if type(list2) != list:
        list2 = [list2]
    min_len = min(len(list1), len(list2))
    for i, (item1, item2) in enumerate(zip(list1, list2)):
        if type(item1) == list or type(item2) == list:
            res = compare(item1, item2)
        else:
            res = item1 - item2
        if res:
            return res
    return len(list1) - len(list2)


with open("day13.txt") as file:
    index = 0
    total_right_oders = 0
    div1 = 1
    div2 = 2
    for block in file.read().split("\n\n"):
        index += 1
        line1, line2 = block.splitlines()
        list1 = eval(f"{line1}") # generate lambda function from string
        list2 = eval(f"{line2}") # generate lambda function from string
        
        if compare(list1, list2) < 0:
            total_right_oders += index
        for item in [list1, list2]:
            if compare(item, [[2]]) < 0:
                div1 += 1
                div2 += 1
            elif compare(item, [[6]]) < 0:
                div2 += 1
print(f"{total_right_oders}")
print(f"{div1 * div2}")
