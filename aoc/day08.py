def check_visibility(tree_arr,len_row, len_col, x, y):
    visible = 1
    if (x == 0 or y == 0 or x == len_row-1 or y == len_col-1):
        return visible
    for i in range(x):
        if tree_arr[i][y] >= tree_arr[x][y]:
            visible = 0
            break
    if visible == 1:
        return visible
    visible = 1
    for i in range(x+1,len_row):
        if tree_arr[i][y] >= tree_arr[x][y]:
            visible = 0
            break
    if visible == 1:
        return visible
    visible = 1
    for j in range(y):
        if tree_arr[x][j] >= tree_arr[x][y]:
            visible = 0
            break
    if visible == 1:
        return visible
    visible = 1
    for j in range(y+1, len_col):
        if tree_arr[x][j] >= tree_arr[x][y]:
            visible = 0
            break
    return visible
def calculate_scenic_score(tree_arr,len_row, len_col, x, y):
    score_down = 0
    score_up = 0
    score_right = 0
    score_left = 0
    if (x == 0 or y == 0 or x == len_row-1 or y == len_col-1):
        return 0
    i = x-1
    while i >= 0:
        score_up += 1
        if tree_arr[i][y] < tree_arr[x][y]:
            i -= 1
        else:
            break
    i = x+1
    while i < len_col:
        score_down += 1
        if tree_arr[i][y] < tree_arr[x][y]:
            i += 1
        else:
            break
    j = y-1
    while j >= 0:
        score_left += 1
        if tree_arr[x][j] < tree_arr[x][y]:
            j -= 1
        else:
            break
    j = y+1
    while j < len_row:
        score_right += 1
        if tree_arr[x][j] < tree_arr[x][y]:
            j += 1
        else:
            break
    return (score_down * score_up * score_left * score_right)

with open("day08.txt") as file:
    lines = file.readlines()
tree_array = []
len_row = len(lines)
len_col = len(lines[0].strip())
for i in range(len_row):
   tree_rows = []
   line = lines[i].strip()
   for j in range(len_col):
       tree_rows.append(int(line[j]))
   tree_array.append(tree_rows)
count_visible = 0
for i in range(len_row):
    for j in range(len_col):
        count_visible += check_visibility(tree_array,len_row, len_col, i, j)

print(count_visible)

max_scenic_score = 0
for i in range(len_row):
    for j in range(len_col):
        scenic_score = calculate_scenic_score(tree_array,len_row, len_col, i, j)
        if scenic_score > max_scenic_score:
            max_scenic_score = scenic_score
print(max_scenic_score)

