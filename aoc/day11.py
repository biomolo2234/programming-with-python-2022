import re
from copy import deepcopy

match = re.compile(r"""Monkey (\d+):
  Starting items: ([^\n]*)
  Operation: new = ([^\n]*)
  Test: divisible by (\d+)
    If true: throw to monkey (\d+)
    If false: throw to monkey (\d+)\n?""").fullmatch

class Monkey:
    def __init__(self, index, items, op, div, on_true, on_false):
        self.index = int(index)
        self.items = [int(item) for item in items.split(", ")]
        self.op = eval(f"lambda old: int({op})") # generate lambda function from string
        self.div = int(div)
        self.on_true = int(on_true)
        self.on_false = int(on_false)
        self.counter = 0

    def throw1(self):
        old_items = self.items
        self.items = []
        for item in old_items:
            self.counter += 1
            val = self.op(item) // 3
            if val % self.div == 0:
                monkeys[self.on_true].items.append(val)
            else:
                monkeys[self.on_false].items.append(val)

    def throw2(self):
        old_items = self.items
        self.items = []
        for item in old_items:
            self.counter += 1
            val = self.op(item) % lcm # least common multiple
            if val % self.div == 0:
                monkeys[self.on_true].items.append(val)
            else:
                monkeys[self.on_false].items.append(val)

def gcd(x, y): #greatest common divisor 
    if x < y:
        x, y = y, x
    while y > 1:
        x, y = y, x % y
    return y

with open("day11.txt") as file:
    monkeys = []
    for block in file.read().split("\n\n"):
        index, items, op, div, on_true, on_false = match(block).groups()
        monkeys.append(Monkey(index, items, op, div, on_true, on_false))
        assert monkeys[-1].index == len(monkeys) - 1

lcm = 1
for monkey in monkeys:
    lcm = lcm * monkey.div // gcd(lcm, monkey.div)

orig_data = deepcopy(monkeys)

for _ in range(20):
    for monkey in monkeys:
        monkey.throw1()

top2 = sorted([monkey.counter for monkey in monkeys])[-2:]
print("%d" % (top2[0] * top2[1]))

monkeys = orig_data

for _ in range(10000):
    for monkey in monkeys:
        monkey.throw2()

top2 = sorted([monkey.counter for monkey in monkeys])[-2:]
print("%d" % (top2[0] * top2[1]))
