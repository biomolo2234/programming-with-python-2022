import re

match = re.compile(r"Valve ([A-Z]+) has flow rate=([0-9]+); tunnels? leads? to valves? ([A-Z ,]+)").fullmatch

with open("day16.txt") as file:
    lines = file.readlines()
for line in lines:
    line = line.strip()
    cave, rate, tunnel_cave = match(line).groups()
