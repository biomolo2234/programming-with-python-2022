from itertools import combinations
with open("day18.txt") as file:
    cubes = []
    for line in file:
        line = line.strip()
        x,y,z = line.split(",")
        cubes.append([int(x), int(y), int(z)])
neighbors = 0
for cube1, cube2 in combinations(cubes, 2):
    if abs(cube1[0]-cube2[0]) + abs(cube1[1]-cube2[1]) + abs(cube1[2]-cube2[2]) == 1 :
        neighbors += 1
all_surface = len(cubes) * 6
print( all_surface - neighbors*2)
