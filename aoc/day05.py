from copy import deepcopy

with open("day05.txt") as file:
    line = file.readline()
    stack_list = []
    stack_num = len(line)//4
    for i in range(stack_num):
        stack_list.append([])
    for i in range(stack_num):
        if line[i*4] != " ":
            stack_list[i].append(line[i*4+1])
    for line in file:
        len_line = len(line)
        if len_line == 1:
            break
        for i in range(stack_num):
            if line[i*4] != " ":
                stack_list[i].append(line[i*4+1])
    for i in range(stack_num): # reverse list
        stack_list[i][::-1]
    org_stack_list = deepcopy(stack_list)
    for line in file:

        stru = line.split()
        num_move = int(stru[1])
        list_add = int(stru[5])-1
        list_remove = int(stru[3])-1
        #org_stack_list[list_add].insert(0,stack_list[list_remove][0:num_move])
        for i in range(num_move):
            stack_list[list_add].insert(0,stack_list[list_remove][0])
            stack_list[list_remove].pop(0)
            org_stack_list[list_add].insert(0,org_stack_list[list_remove][num_move-i-1])
        for i in range(num_move):
            org_stack_list[list_remove].pop(0)



for i in range(stack_num):
    print(stack_list[i][0], end="")
print("\n")
for i in range(stack_num):
    print(org_stack_list[i][0], end="")
print("\n")



