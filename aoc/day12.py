def bfs(start_positions):
    visited = set()
    queue = start_positions
    for point in start_positions:
        visited.add(point)
    
    distance = 0
    while queue:
        next_queue = []
        distance += 1
        for i, j in queue:
            if (i,j) == end_position:
                return distance-1          
            for neighbour in [(i-1, j), (i+1 , j), (i, j-1), (i, j+1)]:
                try:
                    if dic_nodes[neighbour] - dic_nodes[i,j] <= 1:
                        if neighbour not in visited:
                            next_queue.append(neighbour)
                            visited.add(neighbour)
                except KeyError:
                    pass
        queue = next_queue
  
dic_nodes = {}
start_position = (0,0)
end_position = (0,0)
with open("day12.txt") as file:
    lines = file.readlines()
for i,line in enumerate(lines):
    line = line.strip()
    element_line = list(line)
    for j, char in enumerate(element_line):
        if char == 'S':
            start_position = (i,j)
            dic_nodes[i,j] = ord('a')
        elif char == 'E':
            end_position = (i,j)
            dic_nodes[i,j] =ord('z')
        else:
            dic_nodes[i,j] = ord(char)
min_path = bfs([start_position])
print(min_path)

#part2
possible_start_point = []
for item in dic_nodes:
    if dic_nodes[item] == ord('a'):       
        possible_start_point.append(item)
#print(possible_start_point)
print(bfs(possible_start_point))

