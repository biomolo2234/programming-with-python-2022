def move_down(rock_coordinates,sand_coordinate):
    while True:
        new_sand_coor = (sand_coordinate[0],sand_coordinate[1]+1)
        if new_sand_coor in rock_coordinates.keys():
          return new_sand_coor ,sand_coordinate
        if new_sand_coor[1] >= y_max_coordinate:
            return (-1, -1), sand_coordinate
        sand_coordinate = new_sand_coor
        
def move_left(rock_coordinates,sand_coordinate):
    new_sand_coor = (sand_coordinate[0]-1,sand_coordinate[1]+1)
    if sand_coordinate[1] <= y_min_coordinate or sand_coordinate[0] <= x_min_coordinate:
        return (-1, -1), sand_coordinate
    else:
        return new_sand_coor ,sand_coordinate

    
def move_right(rock_coordinates,sand_coordinate):
    new_sand_coor = (sand_coordinate[0]+1,sand_coordinate[1]+1)
    if sand_coordinate[1] <= y_min_coordinate or sand_coordinate[0] >= x_max_coordinate:
        return (-1, -1),sand_coordinate
    else:
        return new_sand_coor ,sand_coordinate
    
def move(rock_coordinates,sand_coordinate):
    while True:
        new_sand_coor, sand_coordinate = move_down(rock_coordinates,sand_coordinate)
        if new_sand_coor == (-1, -1):
            return (-1, -1)      
        new_sand_coor, sand_coordinate = move_left(rock_coordinates,sand_coordinate)
        if new_sand_coor == (-1, -1):
            return (-1, -1)
        if new_sand_coor in rock_coordinates.keys():          
            new_sand_coor, sand_coordinate = move_right(rock_coordinates,sand_coordinate)
        if new_sand_coor == (-1, -1):
            return (-1, -1)
        if new_sand_coor in rock_coordinates.keys():
            return sand_coordinate
        else:
            sand_coordinate = new_sand_coor

def move_down2(rock_coordinates,sand_coordinate):
    while True:
        new_sand_coor = (sand_coordinate[0],sand_coordinate[1]+1)
        if (new_sand_coor in rock_coordinates.keys()) or (new_sand_coor[1] == y_max_coordinate+2):
          return new_sand_coor ,sand_coordinate
        sand_coordinate = new_sand_coor
       
def move_left2(rock_coordinates,sand_coordinate):
    new_sand_coor = (sand_coordinate[0]-1,sand_coordinate[1]+1)
    return new_sand_coor ,sand_coordinate    
def move_right2(rock_coordinates,sand_coordinate):
    new_sand_coor = (sand_coordinate[0]+1,sand_coordinate[1]+1)
    return new_sand_coor ,sand_coordinate
def move2(rock_coordinates,sand_coordinate):
    while True:
        new_sand_coor, sand_coordinate = move_down2(rock_coordinates,sand_coordinate)
        if new_sand_coor[1] == y_max_coordinate+2:
            return sand_coordinate
        new_sand_coor, sand_coordinate = move_left2(rock_coordinates,sand_coordinate)
        if new_sand_coor in rock_coordinates.keys():           
            new_sand_coor, sand_coordinate = move_right2(rock_coordinates,sand_coordinate)
        if new_sand_coor in rock_coordinates.keys():
            return sand_coordinate
        else:
            sand_coordinate = new_sand_coor
    
    
with open("day14.txt") as file:
    lines = file.readlines()

rock_coordinates = {}
x_min_coordinate = 500
y_min_coordinate = 0
x_max_coordinate = 500
y_max_coordinate = 0
sum_rock_same_row = 0
for line in lines:
    points = []
    line = line.strip()
    coordinates = line.split("->")
    for coordinate in coordinates:
        xy = coordinate.split(",")
        x = int(xy[0])
        y = int(xy[1])
        # we calculate min and max coordinate to find start postion of falling forever
        if x < x_min_coordinate:
            x_min_coordinate = x
        if y < y_min_coordinate:
            y_min_coordinate = y
        if x > x_max_coordinate:
            x_max_coordinate = x
        if y > y_max_coordinate:
            y_max_coordinate = y
        points.append([x,y])
    for i in range(len(points)-1):
        start_point = points[i]
        next_point = points[i+1]
        if start_point[0] == next_point[0]:
            for j in range(min(start_point[1],next_point[1]), max(start_point[1],next_point[1])+1):
                rock_coordinates[(start_point[0],j)] = 1
        else:               
            for j in range(min(start_point[0],next_point[0]), max(start_point[0],next_point[0])+1):
                rock_coordinates[(j,start_point[1])] = 1

num_units = 0
while True:
    sand_coordinate = (500,0)
    new_sand_coordinate = move(rock_coordinates,sand_coordinate)
    if new_sand_coordinate == (-1, -1) :
        break
    rock_coordinates[new_sand_coordinate] = 1   
    num_units += 1
print(num_units)

#part2
while True:
    sand_coordinate = (500,0)
    num_units += 1
    new_sand_coord = move2(rock_coordinates,sand_coordinate)
    if new_sand_coord == (-1, -1) or (new_sand_coord in rock_coordinates.keys()) or new_sand_coord == (500,0):
        break
    rock_coordinates[new_sand_coord] = 1        
print(num_units)
