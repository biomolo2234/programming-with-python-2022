dic_dir = {}
size_dirs = []
current_dir = ""
with open("day07.txt") as file:
    lines = file.readlines()

for line in lines:
    line = line.strip()
    command = line.split()
    if command[1] == "cd":
        if command[2] == ".." :
            size_current_dir = sum(item[1] for item in size_dirs if item[0] == current_dir)
            dic_dir[current_dir] = size_current_dir
            size_dirs = [item for item in size_dirs if item[0] != current_dir] # like remove item with current directory
            index = current_dir.rindex("/")
            current_dir = current_dir[:index]
            index = current_dir.rindex("/")
            current_dir = current_dir[:index+1]
            size_dirs.append([current_dir, size_current_dir])
        else:
            current_dir = current_dir + command[2] + "/"
    if command[0].isdigit():
        size_dirs.append([current_dir, int(command[0])])
        
while True:
    print(current_dir)
    
    index = current_dir.rfind("/")
    if index == -1:
        break
    size_current_dir = sum(item[1] for item in size_dirs if item[0] == current_dir)
    if current_dir in dic_dir:
        dic_dir[current_dir] = size_current_dir + dic_dir[current_dir]
    else:
        dic_dir[current_dir] = size_current_dir
    size_dirs = [item for item in size_dirs if item[0] != current_dir]

    current_dir = current_dir[:index]
    index = current_dir.rfind("/")
    current_dir = current_dir[:index+1]
    size_dirs.append([current_dir, size_current_dir])
    if current_dir == "/":
        break
#print(dic_dir)

sum_size_dir = sum(val for val in dic_dir.values() if val < 100000)
print(sum_size_dir)

needed_space = dic_dir["//"]- 40000000
smallest_dir_size = dic_dir["//"]
for val in dic_dir.values():
    if val >= needed_space:
        if val < smallest_dir_size:
            smallest_dir_size = val
print(smallest_dir_size)
    
    
