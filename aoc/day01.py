
max_energy1 = 0
max_energy2 = 0
max_energy3 = 0
energy = 0
with open("day01.txt") as file:
    for line in file:
        if line == "\n":
            if energy > max_energy1:
                max_energy3 = max_energy2
                max_energy2 = max_energy1
                max_energy1 = energy
            elif energy > max_energy2:
                max_energy3 = max_energy2
                max_energy2 = energy
            elif energy > max_energy3:
                max_energy3 = energy
            energy = 0
        else:
            energy += int(line)
print(max_energy1)
print(max_energy1 + max_energy2 + max_energy3)
