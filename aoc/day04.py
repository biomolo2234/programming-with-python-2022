
count_same_assignment = 0
count_overlap_assignment = 0

with open("day04.txt") as file:
    for line in file:
        line = line.strip()
        pairs = line.split(",")
        elf1 = pairs[0].split("-")
        elf2 = pairs[1].split("-")
        if((int(elf1[0]) >= int(elf2[0]))and(int(elf1[1]) <= int(elf2[1]))
           or (int(elf1[0]) <= int(elf2[0]))and(int(elf1[1]) >= int(elf2[1]))):
            count_same_assignment += 1
        if not (int(elf1[1]) < int(elf2[0]) or int(elf1[0]) > int(elf2[1])):
            count_overlap_assignment += 1

print(count_same_assignment)
print(count_overlap_assignment)



