from copy import deepcopy

def monkeyopreation(monkey_name):
    monkey_name = monkey_name.strip()
    monkey_op = monkey_dic[monkey_name]
    if not isinstance(monkey_op, str):
        return monkey_op
    if monkey_op.isdigit():
        monkey_dic[monkey_name] = int(monkey_op)
        return int(monkey_op)
    else:
        new_monkey = monkey_op.split()
        if new_monkey[1] == "+":
            res = monkeyopreation(new_monkey[0])+ monkeyopreation(new_monkey[2])
        elif new_monkey[1] == "-":
            res = monkeyopreation(new_monkey[0])- monkeyopreation(new_monkey[2])
        elif new_monkey[1] == "*":
            res = monkeyopreation(new_monkey[0])* monkeyopreation(new_monkey[2])
        elif new_monkey[1] == "/":
            res = monkeyopreation(new_monkey[0]) / monkeyopreation(new_monkey[2])
        monkey_dic[monkey_name] = res
        return res

monkey_dic = {}
with open("day21.txt") as file:
    lines = file.readlines()
for line in lines:
    line = line.strip()
    monkey_name, mankey_op = line.split(":")
    if not monkey_name.isidentifier():
        print(f"Monkey {monkey_name} is not identifier")
    monkey_dic[monkey_name.strip()] = mankey_op.strip()

monkey_dic_copy = deepcopy(monkey_dic)
val = monkeyopreation("root")
print(val)
monkey_dic = monkey_dic_copy
monkey1, _, monkey2 = monkey_dic["root"].split()
monkey_dic["humn"] = 1j #complex number, we don't need to calculate main operations(+,*,-,/)
# all numbers are in this form: a*humn + b
# we store those number as a complex number: a*j + b
val = monkeyopreation(monkey1) - monkeyopreation(monkey2)
a = val.imag
b = val.real
# a*humn + b = 0
print(-b / a)
